CC  := clang
BIN := bin
OBJ := obj
SRC := src

SRCS := $(wildcard $(SRC)/*.c)
OBJS := $(patsubst $(SRC)/%.c, $(OBJ)/%.o, $(SRCS))
EXEC := $(BIN)/compiler

CFLAGS := -Wall

.PHONY: all run clean

all: $(EXEC)

$(EXEC): $(OBJS) | $(BIN)
	$(CC) $^ -o $@

$(OBJ)/%.o: $(SRC)/%.c | $(OBJ)
	$(CC) $(CFLAGS) -c $< -o $@

$(BIN) $(OBJ):
	mkdir $@

run: $(EXEC)
	-$< input/input01
	-$< input/input02
	-$< input/input03
	-$< input/input04
	-$< input/input05

clean:
	rm -fr bin/compiler obj/*.o
