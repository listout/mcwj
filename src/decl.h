// function protoptype for all compiler files
int scan(struct token *t);

struct ast_node *mkastnode(int op, struct ast_node *left, struct ast_node *right, int intvalue);
struct ast_node *mkastleaf(int op, int intvalue);
struct ast_node *mkastunary(int op, struct ast_node *left, int intvalue);
struct ast_node *binexpr(void);
int interpret_ast(struct ast_node *n);
