#include "defs.h"
#include "data.h"
#include "decl.h"

// Get the next character from the input file
static int
next(void)
{
	int c;

	if(Putback) // use the character putback if there is one
	{
		c       = Putback;
		Putback = 0;
		return c;
	}

	c = fgetc(Infile);    // read from input file
	if('\n' == c) Line++; // increment file count
	return c;
}

static void
putback(int c)
{
	Putback = c;
}

// return the position of character c in string s
// or -1 if c not found
static int
chrpos(char *s, int c)
{
	char *p;

	p = strchr(s, c);
	return (p ? p - s : -1);
}

// scan and return an integer literal values from the input file.
// Store the value as a string in text.
static int
scanint(int c)
{
	int k, val = 0;

	//convert each character into an int value
	while((k = chrpos("0123456789", c)) >= 0)
	{
		val = val * 10 + k;
		c   = next();
	}

	//we hit a non-integer character, put it back
	putback(c);
	return val;
}

// skip past input that we don't need to deal with
// for example whitespace, newlines. Return the first
// character we need to deal with.
static int
skip(void)
{
	int c;
	c = next();
	while(c == ' ' || c == '\t' || c == '\n' || c == '\r' || c == '\f')
	{
		c = next();
	}
	return c;
}

// scan and return the next token found int the input
// 1 for a valid token and 0 for invalid token
int
scan(struct token *t)
{
	int c;

	//skip not needed character
	c = skip();

	//determine token based on input character
	switch(c)
	{
	case EOF: t->token = t_eof; return 0;
	case '+': t->token = t_plus; break;
	case '-': t->token = t_minus; break;
	case '/': t->token = t_slash; break;
	case '*': t->token = t_star; break;
	default:
		//if it's a digit, scan the literal integer value in it
		if(isdigit(c))
		{
			t->intvalue = scanint(c);
			t->token    = t_intlit;
			break;
		}

		printf("Unrecognized character %c on line %d\n", c, Line);
		exit(1);
	}

	return 1;
}
