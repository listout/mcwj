#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

// Token structure
struct token
{
	int token;
	int intvalue;
};

// tokens
enum
{
	t_plus,
	t_minus,
	t_star,
	t_slash,
	t_intlit,
	t_eof
};

// ast node types
enum
{
	a_add,
	a_subtract,
	a_multiply,
	a_divide,
	a_intlit
};

// ast structure
struct ast_node
{
	int op;                 // operation to be performed on the tree
	struct ast_node *left;  // left child
	struct ast_node *right; // right child
	int intvalue;           // for a_intlit
};
