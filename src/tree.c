#include "defs.h"
#include "data.h"
#include "decl.h"

// ast tree function

// build and return a generic ast node
struct ast_node *
mkastnode(int op, struct ast_node *left, struct ast_node *right, int intvalue)
{
	struct ast_node *n;

	// malloc a new ast node
	n = (struct ast_node *)malloc(sizeof(struct ast_node));
	if(n == NULL)
	{
		fprintf(stderr, "Unable to malloc in mkastnode()\n");
		exit(1);
	}

	// copy in the field values and return 1
	n->op       = op;
	n->left     = left;
	n->right    = right;
	n->intvalue = intvalue;
	return n;
}

// make an ast leaf node
struct ast_node *
mkastleaf(int op, int intvalue)
{
	return (mkastnode(op, NULL, NULL, intvalue));
}

// make an ast node with only one child
struct ast_node *
mkastunary(int op, struct ast_node *left, int intvalue)
{
	return (mkastnode(op, left, NULL, intvalue));
}
