#include "defs.h"
#include "data.h"
#include "decl.h"

// parsing of expression

// ast node reresenting it
static struct ast_node *
primary(void)
{
	struct ast_node *n;

	// for an intlit token, make a leaf ast node for it and scan in the next token.
	// Otherwise, a syntax error for any other token type
	switch(Token.token)
	{
	case t_intlit:
		n = mkastleaf(a_intlit, Token.intvalue);
		scan(&Token);
		return n;
	default: fprintf(stderr, "syntax error on line %d\n", Line); exit(1);
	}
}

// convert token to ast operation
int
arithop(int tok)
{
	switch(tok)
	{
	case t_plus: return a_add;
	case t_minus: return a_subtract;
	case t_star: return a_multiply;
	case t_slash: return a_divide;
	default: fprintf(stderr, "unknown token in arithop() on line %d\n", Line); exit(1);
	}
}

struct ast_node *
multiplicative_expr()
{
	struct ast_node *left, *right;
	int tokentype;

	// get the integer literal on the left
	// and fetch the next token at the same time
	left = primary();

	// if not tokens are left, return just the left node
	tokentype = Token.token;
	if(tokentype == t_eof) return left;

	// while the token is '*' or '/'
	while((tokentype == t_star) || (tokentype == t_slash))
	{
		//fetch the next integer literal
		scan(&Token);
		right = primary();

		// join with the left interger literal
		left = mkastnode(arithop(tokentype), left, right, 0);

		// update thee details of the current token
		// if no tokens are left, return just the left node
		tokentype = Token.token;
		if(tokentype == t_eof) break;
	}

	return left;
}

// return as ast whose root is a '-' or '+' binary operator
struct ast_node *
additive_expr(void)
{
	struct ast_node *left, *right;
	int tokentype;

	// get the left sub-tree at a higher precedence
	left = multiplicative_expr();

	// if no tokens are left, retun only left node
	tokentype = Token.token;
	if(tokentype == t_eof) return left;

	// loop working on token at given level of precedence
	while(1)
	{
		// fetch in the next integer literal
		scan(&Token);

		// get the right sub-tree at a higher level of precedence
		right = multiplicative_expr();

		// join the two sub-trees with low precedence operator
		left = mkastnode(arithop(tokentype), left, right, 0);

		// get token at ginve precedence level
		tokentype = Token.token;
		if(tokentype == t_eof) break;
	}
	return left;
}

// return an ast tree whose root is a binary operator
struct ast_node *
binexpr(void)
{
	return additive_expr();
}
