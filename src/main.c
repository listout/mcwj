#include "defs.h"
#define extern_
#include "data.h"
#undef extern_
#include "decl.h"
#include <errno.h>

// initialize global variables
static void
init()
{
	Line    = 1;
	Putback = '\n';
}

// usage if started incorrectly
static void
usage(char *prog)
{
	fprintf(stderr, "Usage: %s infile\n", prog);
	exit(1);
}

int
main(int argc, char *argv[])
{
	struct ast_node *n;

	if(argc != 2) usage(argv[0]);

	init();

	if((Infile = fopen(argv[1], "r")) == NULL)
	{
		fprintf(stderr, "Uable to open %s, %s\n", argv[1], strerror(errno));
		exit(1);
	}

	scan(&Token);
	n = binexpr();
	printf("%d\n", interpret_ast(n));

	exit(0);
}
