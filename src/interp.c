#include "defs.h"
#include "data.h"
#include "decl.h"

// ast interpreter

// list of ast operator
static char *ast_top[] = {"+", "-", "*", "/"};

// given an ast, interpert the operator in it
// and return a final value
int
interpret_ast(struct ast_node *n)
{
	int leftval, rightval;

	// get the left and right sub-tree value
	if(n->left) leftval = interpret_ast(n->left);
	if(n->right) rightval = interpret_ast(n->right);

	// debug options
	if(n->op == a_intlit)
		printf("int %d\n", n->intvalue);
	else
		printf("%d %s %d\n", leftval, ast_top[n->op], rightval);

	switch(n->op)
	{
	case a_add: return leftval + rightval;
	case a_subtract: return leftval - rightval;
	case a_multiply: return leftval * rightval;
	case a_divide: return leftval / rightval;
	case a_intlit: return n->intvalue;
	default: printf(stderr, "unknown ast operator %d\n", n->op); exit(1);
	}
}
